using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    private Rigidbody rigidbody;

    private float startHeight;

    private float jumpTime;

    [Header("JumpSettings")]
    [SerializeField] private float jumpHigh;
    [SerializeField] private float jumpLength;
    [SerializeField] private float timeAdditionalPowerJump;
    



   private bool isJumped;

    private void Awake()
    {
        rigidbody = GetComponent<Rigidbody>();
    }


    private void FixedUpdate()
    {
       if (Input.GetMouseButton(0) /*&& !isJumped*/)
        {
            Jump();
        }

       //if (Input.GetMouseButtonUp(0))
       // {
       //     isJumped = false;
       // }

        CheckGround();
    }

    private void Jump()
    {
        Debug.LogError($"jumpHeight = {jumpHigh}, startH = {startHeight}, traPos = {transform.position.y} , {transform.position.y >= jumpHigh + startHeight} ");
        if (transform.position.y >= jumpHigh + startHeight)
        {
            isJumped = true;
        }

        jumpTime += Time.deltaTime;
        if (jumpTime < timeAdditionalPowerJump)
        {
            rigidbody.velocity = new Vector3(1, jumpHigh, jumpLength);
            //rigidbody.AddForce(new Vector3(0, jumpHigh, jumpLength) * Time.deltaTime) ;
        }
       
        //else
        //{
        //    jumpTime = 0;
        //}

        //rigidbody.velocity = new Vector3(1 , 1, 1) * jumpLength;
    }


    private void CheckGround()
    {
        if (transform.position.y <= 0.2)
        {
            jumpTime = 0;
        }
    }

   
}
